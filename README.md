# random_ascii

> A command line utility that will randomly choose display an ascii-art file
> from your collection, taking care to discard those that do not fit in your current window

`random_ascii` is a simple binary that picks a random uncolored ascii art file

in your collection, which **fits in your current terminal window** by checking

terminal size and file size, considering the prompt-height to subtract to the

window height, so the chosen file will be entirely visible (no crop, no gap),

since you don't resize the window.


Concerning the "renderer" binary, you can :

- let the default `dotacat` renderer (requires `dotacat`)
- avoid the "rainbow colors" and `dotacat` dependency using `cat`
- avoid to use cargo using the binary you want (eg : `lolcat`...)


File sizes are fully parsed just one time, and cached in fileDB, to offer better

performance and avoid unnecessary disk accesses...

- GNU General Public License, version 3 (GPL-3.0)
- currently tested on Debian/Ubuntu, Arch/Manjaro
- testers and feedbacks are welcome !

## Breaking changes since v0.1.3

Due to the use of `clap` to normalize and enhance arguments parsing, the `-h`

parameter is now reserved to `--help` and no longer for the `--prompt-height`.

So please use `-s` if you want the shorter one.


---

## HOW TO - INSTALL

### The easiest way (with cargo)

Install `dotacat` or a renderer of your choice, and `random_ascii`

NB : Cargo binaries directory must be part of your `$PATH`

(eg : `$HOME/.cargo/bin`)

```shell
cargo install dotacat
cargo install random_ascii
```

### Manually (if you don't want to use cargo)

Just put the binary somewhere in your `$PATH`,

(eg : `/usr/local/bin`)

```shell
sudo curl -fSsL https://gitlab.com/flashbios/rust_random_ascii/-/raw/main/latest_release/random_ascii?inline=false -o /usr/local/bin/random_ascii
sudo chmod +x /usr/local/bin/random_ascii
# optional 
sudo curl -fSsL https://gitlab.com/flashbios/rust_random_ascii/-/raw/main/latest_release/dotacat?inline=false -o /usr/local/bin/dotacat
sudo chmod +x /usr/local/bin/dotacat
```

---

## HOW TO - USE

### Case 1 : you're already ok with the default options

- I want to use `dotacat` which is already installed
- my ascii directory is writable and at `${HOME}/.config/random_ascii/`
- all my ascii files have the `.ascii` extension
- my prompt is just 1 line height

```shell
random_ascii
```

### Case 2 : you may want to change some option

- `-h`, `--help`
  - just print help


- `-V`, `--version`
  - just print version


- `-p <PATH>`, `--path <PATH>`
  - (default is relative to `$HOME/` : `.config/random_ascii`)

  - relative : from your HOME directory, without leading or trailing slashes

    (eg : `--path Documents/path/to/random_ascii_files`)

  - absolute : must be readable for your user !

    (eg : `--path /opt/random_ascii_files`)


- `-e <EXTENSION>`, `--extension <EXTENSION>`
  - if you don't want to mass rename your collection

    (eg : `--extension txt`)


- `-s <PROMPT_HEIGHT>`, `--prompt-height <PROMPT_HEIGHT>`
  - will be subtracted to the height of the terminal window,

    to filter out files that are too tall (default = 1)


- `-r`, `--reset-cache`
  - reset cache db (just use once after updates, to force parsing files again)

    with obvious lower performance but fresh data


- `-d <DISPLAY_RENDERER>`, `--display-renderer <DISPLAY_RENDERER>`
  - if you prefer `cat`, `lolcat`, `dot2acat` or whatever is installed...

```shell
random_ascii -p /opt/random_ascii_files -e txt -d cat -s 4 -r
```

When you're ok with the command options, you can append it to your `.bashrc`,

`.zshrc` (fits well just before a starship prompt)... to enjoy seeing it either

in new tabs and splits.

---

## HOW TO - TEST WITHOUT INSTALLING

Just clone/update or download the latest version, and use the binaries,

playing with the arguments, and the `samples` directory :

```shell
cd "${HOME}/Downloads/random_ascii/latest_release/"

./random_ascii -r -d "${HOME}/Downloads/random_ascii/latest_release/dotacat" -p "${HOME}/Downloads/random_ascii/samples/"
```

> NB : Do not forget to `--reset-cache` at least one time after that,
> 
> if you install for real and use the defaut `--path` option.