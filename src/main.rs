
mod error;

extern crate rustbreak;
extern crate serde;

use clap::Parser;
use home::home_dir;
use rand::Rng;
use rustbreak::{Database, FileDatabase, deser::Yaml, backend::FileBackend};
use serde::{Serialize, Deserialize};
use std::{
    process::Command,
    fs::{File, read_dir, create_dir_all},
    io::{BufRead, BufReader, ErrorKind},
    path::{Path, PathBuf},
    string::{String, ToString},
    collections::HashMap
};
use terminal_size::{Width, Height, terminal_size};

pub use self::error::Result;

const COMMON_CONFIG: &str = ".config";
const PROGRAM_NAME: &str = "random_ascii";
const CACHE_FILENAME: &str = "cache.yaml";
const DEFAULT_EXTENSION: &str = "ascii";
const DEFAULT_TERM_SIZE: (usize, usize) = (100, 24);

#[derive(Parser)]
#[command(
author = "flashbios",
about = "A command line utility that will randomly display an ascii-art file from your collection, taking care to discard those that do not fit in your current window",
version,
after_help = "Example:\n\trandom_ascii -r -d cat -s 3 -p Documents/files_path -e txt\n\t\tReset db cache, use `cat` as renderer\n\t\tbe aware that my prompt is 3 lines height\n\t\tto display an appropriate file from ~/Documents/files_path/**.txt"
)]
struct CmdOpts {
    #[arg(short = 'p', long, default_value = "")]
    /// path
    path: String,
    /// extension
    #[arg(short = 'e', long, default_value = DEFAULT_EXTENSION)]
    extension: String,
    /// prompt_height
    #[arg(short = 's', long, default_value = "1")]
    prompt_height: usize,
    /// reset_cache
    #[arg(short = 'r', long, num_args = 0..=1, require_equals = true, default_value = "false", default_missing_value = "true")]
    reset_cache: String,
    /// display_renderer
    #[arg(short = 'd', long, default_value = "dotacat")]
    display_renderer: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct AsciiFile {
    path: String,
    cols: usize,
    lines: usize,
}
impl AsciiFile {
    fn matches_term_size(&self, tw: &usize, th: &usize, ph: &usize) -> bool {
        self.cols <= *tw && self.lines <= th - ph
    }
}

fn do_main() -> Result<()> {
    let opts: CmdOpts = CmdOpts::parse();
    // Get terminal size or fallback to DEFAULT_TERM_SIZE
    let (terminal_width, terminal_height) = match terminal_size() {
        Some((Width(w), Height(h))) => (w as usize, h as usize),
        None => DEFAULT_TERM_SIZE
    };
    // Get home dir (required)
    let home = match home_dir() {
        Some(path) => path.display().to_string(),
        None => return Err("Unable to find your home directory !".into())
    };
    // Get config_dir (required)
    let config_dir = Path::new(&home).join(COMMON_CONFIG).join(PROGRAM_NAME);
    if let Err(e) = create_dir_all(&config_dir) {
        if e.kind() != ErrorKind::AlreadyExists {
            let error = e.to_string();
            let e_path = config_dir.as_path().to_str().unwrap();
            return Err(format!("Unable to create data dir at : {e_path} \n{error}").into());
        }
    }
    // Get ascii_rep (required)
    let ascii_rep = match opts.path.is_empty() {
        true => config_dir.clone().to_owned(),
        false => {
            let c_path = Path::new(&opts.path);
            match c_path.is_absolute() {
                true => c_path.to_owned(),
                false => Path::new(&home).join(&opts.path)
            }
        }
    };
    // Load db
    let db_path = config_dir.to_owned().join(CACHE_FILENAME);
    let db = create_or_load_db(db_path)?;
    let db_len = get_db_len(&db);
    // parse files and write db only if required
    if opts.reset_cache == "true" || db_len < 1 {
        if db_len > 0 {
            truncate_db(&db, db_len);
        }
        let ascii_list = get_ascii_list(&ascii_rep, &opts.extension)?;
        refresh_database(&db, ascii_list);
    }
    // read db
    match db.load() {
        Ok(_) => {}
        Err(e) => { return Err(e.into()); }
    };
    // filter db
    let filtered = filter_db(&db, terminal_width, terminal_height, opts.prompt_height)?;

    if !filtered.is_empty() {
        // get a random one if we have some choice
        let random_index = match filtered.len() {
            1 => 0,
            _ => { rand::thread_rng().gen_range(0..filtered.len()) }
        };
        let path_arg = filtered[random_index].path.as_str();
        // command
        let mut command = Command::new(&opts.display_renderer);
        command.arg(path_arg);
        if let Ok(mut child) = command.spawn() {
            child.wait().expect("command wasn't running");
        } else {
            return Err(format!("{} was not found", &opts.display_renderer).into());
        }
    }
    Ok(())
}

fn create_or_load_db(db_path: PathBuf) -> Result<Database<HashMap<usize, AsciiFile>, FileBackend, Yaml>> {
    let db = FileDatabase::<HashMap<usize, AsciiFile>, Yaml>::load_from_path_or_default(db_path)?;
    Ok(db)
}

fn get_db_len(db:&Database<HashMap<usize, AsciiFile>, FileBackend, Yaml>) -> usize {
    db.read(|db| db.len()).unwrap_or(0)
}

fn truncate_db(db:&Database<HashMap<usize, AsciiFile>, FileBackend, Yaml>, db_len: usize) {
    for i in 0..db_len {
        db.write(|db| {
            db.remove_entry(&i );
        }).expect("Db: error removing row");
    }
    db.save().expect("Db: error truncating db");
}

fn get_ascii_list(ascii_rep: &PathBuf, ascii_extension: &String) -> Result<Vec<PathBuf>> {
    let handle_list = read_dir(ascii_rep)?;
    let mut ascii_list: Vec<PathBuf> = handle_list
        .filter_map(|entry| {
            entry.ok().and_then(|f| {
                f.path()
                    .extension()
                    .filter(|ext| ext.to_str() == Some(ascii_extension))
                    .map(|_| f.path())
            })
        }).collect();
    ascii_list.sort();
    Ok(ascii_list)
}

fn refresh_database(db:&Database<HashMap<usize, AsciiFile>, FileBackend, Yaml>, ascii_list: Vec<PathBuf>) {
    db.write(|db| {
        ascii_list.into_iter().enumerate().for_each(|i|{
            if let Ok(parsed_file) = parse_file(&i.1) { db.insert(i.0, parsed_file); }
        });
    }).expect("Db: error writing row");
    db.save().expect("Db: error saving db");
}

fn parse_file(s_file:&PathBuf) -> Result<AsciiFile> {
    let f = File::open(s_file)?;
    let file = BufReader::new(&f);
    let (mut file_cols, mut file_lines):(usize,usize) = (0,0);
    for line in file.lines() {
        let current_line_cols = line.unwrap().len();
        if current_line_cols > file_cols {
            file_cols = current_line_cols
        }
        file_lines+=1;
    }
    let path = s_file.display().to_string();
    Ok(AsciiFile{ path, cols: file_cols, lines: file_lines })
}

fn filter_db(
    db:&Database<HashMap<usize, AsciiFile>, FileBackend, Yaml>,
    terminal_width:usize,
    terminal_height:usize,
    ascii_prompt_height:usize
) -> Result<Vec<AsciiFile>> {
    let mut filtered:Vec<AsciiFile> = Vec::new();
    db.read(|db| {
        filtered = db.values()
                .filter(|&x| x.matches_term_size(&terminal_width, &terminal_height, &ascii_prompt_height))
                .cloned()
                .collect();
    }).expect("Unable to read DB");
    Ok(filtered)
}

fn main() {
    match do_main() {
        Ok(_) => {},
        Err(e) => println!("{}", e)
    }
}
