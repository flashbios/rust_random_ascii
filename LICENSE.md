# GNU General Public License, version 3 (GPL-3.0)

This is the text of the GNU General Public License version 3 (GPL-3.0). You can find the full text of the license at [https://www.gnu.org/licenses/gpl-3.0.html](https://www.gnu.org/licenses/gpl-3.0.html).

## Additional Terms for the GNU GPL version 3

1. **Additional Permissions**: Exceptions and additional permissions to the GNU GPL version 3, covering a wide variety of types of use, are provided by the [GNU Affero General Public License version 3](https://www.gnu.org/licenses/agpl-3.0.html), the [GNU Lesser General Public License version 3](https://www.gnu.org/licenses/lgpl-3.0.html), and the [GNU Free Documentation License version 1.3](https://www.gnu.org/licenses/fdl-1.3.html). That page also describes how to apply these permissions to your work.

2. **Our Recommendations**: Our [guidelines for how the GNU GPL should be applied to your own works](https://www.gnu.org/licenses/gpl-howto.html) are designed to keep it easy to run your works, and to encourage cooperation with other free software projects.

3. **Common Practices**: Here's [how to avoid common problems with the GNU GPL](https://www.gnu.org/licenses/gpl-faq.html). We recommend enforcing your license.

4. **Frequently Asked Questions**: The [GNU GPL Frequently Asked Questions](https://www.gnu.org/licenses/gpl-faq.html) has answers to many common questions about the GNU GPL.

5. **The GNU Project's Perspective**: You may also find more information about the GNU GPL at the [GNU Project's web site](https://www.gnu.org/licenses/gpl-3.0.html).

6. **Helping Further**: The [Free Software Foundation](https://www.fsf.org) has put a great deal of work into helping people understand the GNU GPL. If you need more help, please [contact us](https://www.fsf.org/about/contact.html).

By publishing this file, you agree to the terms and conditions set forth in the GNU General Public License version 3.

---

Original project :
- `random_ascii`
- [https://gitlab.com/flashbios/rust_random_ascii](https://gitlab.com/flashbios/rust_random_ascii)
